﻿namespace LanchoneteNaturalApp.Telas.Frms
{
    partial class FrmTelaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTelaPrincipal));
            this.btnFuncionario = new System.Windows.Forms.Button();
            this.btnEstoque = new System.Windows.Forms.Button();
            this.bntCardapio = new System.Windows.Forms.Button();
            this.btnOrdemDePedido = new System.Windows.Forms.Button();
            this.btnFornecedor = new System.Windows.Forms.Button();
            this.bntCliente = new System.Windows.Forms.Button();
            this.btnCCliente = new System.Windows.Forms.Button();
            this.BtnCFuncionario = new System.Windows.Forms.Button();
            this.btnLogoff = new System.Windows.Forms.Button();
            this.btnCCardapio = new System.Windows.Forms.Button();
            this.btnCPedido = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btninicio = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.ucConsultarPedidos3 = new LanchoneteNaturalApp.Telas.UserControls.UCConsultarPedidos();
            this.ucConsultarPedidos2 = new LanchoneteNaturalApp.Telas.UserControls.UCConsultarPedidos();
            this.ucConsultarPedidos1 = new LanchoneteNaturalApp.Telas.UserControls.UCConsultarPedidos();
            this.ucPedidoCliente1 = new LanchoneteNaturalApp.Telas.UserControls.UCPedidoCliente();
            this.ucFornecedor1 = new LanchoneteNaturalApp.Telas.UserControls.UCFornecedor();
            this.ucEstoque2 = new LanchoneteNaturalApp.Telas.UserControls.UCEstoque();
            this.ucEstoque1 = new LanchoneteNaturalApp.Telas.UserControls.UCEstoque();
            this.ucConsultaFuncionario1 = new LanchoneteNaturalApp.Telas.UserControls.UCConsultaFuncionario();
            this.ucCadastrofuncionario1 = new LanchoneteNaturalApp.Telas.UserControls.UCCadastrofuncionario();
            this.ucConsultaCliente1 = new LanchoneteNaturalApp.Telas.UserControls.UCConsultaCliente();
            this.ucCadastroCliente1 = new LanchoneteNaturalApp.Telas.UserControls.UCCadastroCliente();
            this.ucConsultarCardapio1 = new LanchoneteNaturalApp.Telas.UserControls.UCConsultarCardapio();
            this.ucCadastroCardapio1 = new LanchoneteNaturalApp.Telas.UserControls.UCCadastroCardapio();
            this.ucMain1 = new LanchoneteNaturalApp.Telas.UserControls.UCMain();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnFuncionario
            // 
            this.btnFuncionario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFuncionario.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFuncionario.ForeColor = System.Drawing.Color.Black;
            this.btnFuncionario.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnFuncionario.Location = new System.Drawing.Point(30, 209);
            this.btnFuncionario.Name = "btnFuncionario";
            this.btnFuncionario.Size = new System.Drawing.Size(116, 33);
            this.btnFuncionario.TabIndex = 25;
            this.btnFuncionario.Text = "Funcionario";
            this.btnFuncionario.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFuncionario.UseVisualStyleBackColor = true;
            this.btnFuncionario.Click += new System.EventHandler(this.btnFuncionario_Click);
            // 
            // btnEstoque
            // 
            this.btnEstoque.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEstoque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstoque.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEstoque.ForeColor = System.Drawing.Color.Black;
            this.btnEstoque.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnEstoque.Location = new System.Drawing.Point(30, 287);
            this.btnEstoque.Name = "btnEstoque";
            this.btnEstoque.Size = new System.Drawing.Size(116, 33);
            this.btnEstoque.TabIndex = 23;
            this.btnEstoque.Text = "Estoque";
            this.btnEstoque.UseVisualStyleBackColor = true;
            this.btnEstoque.Click += new System.EventHandler(this.btnEstoque_Click);
            // 
            // bntCardapio
            // 
            this.bntCardapio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bntCardapio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bntCardapio.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntCardapio.ForeColor = System.Drawing.Color.Black;
            this.bntCardapio.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bntCardapio.Location = new System.Drawing.Point(30, 53);
            this.bntCardapio.Name = "bntCardapio";
            this.bntCardapio.Size = new System.Drawing.Size(116, 33);
            this.bntCardapio.TabIndex = 15;
            this.bntCardapio.Text = "Cardápio";
            this.bntCardapio.UseVisualStyleBackColor = true;
            this.bntCardapio.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnOrdemDePedido
            // 
            this.btnOrdemDePedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOrdemDePedido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrdemDePedido.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrdemDePedido.ForeColor = System.Drawing.Color.Black;
            this.btnOrdemDePedido.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnOrdemDePedido.Location = new System.Drawing.Point(30, 365);
            this.btnOrdemDePedido.Name = "btnOrdemDePedido";
            this.btnOrdemDePedido.Size = new System.Drawing.Size(116, 33);
            this.btnOrdemDePedido.TabIndex = 28;
            this.btnOrdemDePedido.Text = "Ordem de Ped.";
            this.btnOrdemDePedido.UseVisualStyleBackColor = true;
            this.btnOrdemDePedido.Click += new System.EventHandler(this.BtnOrdemDePedido_Click);
            // 
            // btnFornecedor
            // 
            this.btnFornecedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFornecedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFornecedor.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFornecedor.ForeColor = System.Drawing.Color.Black;
            this.btnFornecedor.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnFornecedor.Location = new System.Drawing.Point(30, 326);
            this.btnFornecedor.Name = "btnFornecedor";
            this.btnFornecedor.Size = new System.Drawing.Size(116, 33);
            this.btnFornecedor.TabIndex = 29;
            this.btnFornecedor.Text = "Fornecedor";
            this.btnFornecedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFornecedor.UseVisualStyleBackColor = true;
            this.btnFornecedor.Click += new System.EventHandler(this.BtnFornecedor_Click);
            // 
            // bntCliente
            // 
            this.bntCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bntCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bntCliente.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntCliente.ForeColor = System.Drawing.Color.Black;
            this.bntCliente.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bntCliente.Location = new System.Drawing.Point(30, 131);
            this.bntCliente.Name = "bntCliente";
            this.bntCliente.Size = new System.Drawing.Size(116, 33);
            this.bntCliente.TabIndex = 30;
            this.bntCliente.Text = "Cliente";
            this.bntCliente.UseVisualStyleBackColor = true;
            this.bntCliente.Click += new System.EventHandler(this.Cad_Cliente_Click);
            // 
            // btnCCliente
            // 
            this.btnCCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCCliente.Font = new System.Drawing.Font("Poor Richard", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCCliente.ForeColor = System.Drawing.Color.Black;
            this.btnCCliente.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCCliente.Location = new System.Drawing.Point(30, 170);
            this.btnCCliente.Name = "btnCCliente";
            this.btnCCliente.Size = new System.Drawing.Size(116, 33);
            this.btnCCliente.TabIndex = 31;
            this.btnCCliente.Text = "Cons. Cliente";
            this.btnCCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCCliente.UseVisualStyleBackColor = true;
            this.btnCCliente.Click += new System.EventHandler(this.button2_Click);
            // 
            // BtnCFuncionario
            // 
            this.BtnCFuncionario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnCFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnCFuncionario.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCFuncionario.ForeColor = System.Drawing.Color.Black;
            this.BtnCFuncionario.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnCFuncionario.Location = new System.Drawing.Point(30, 248);
            this.BtnCFuncionario.Name = "BtnCFuncionario";
            this.BtnCFuncionario.Size = new System.Drawing.Size(116, 33);
            this.BtnCFuncionario.TabIndex = 32;
            this.BtnCFuncionario.Text = "Cons. Func";
            this.BtnCFuncionario.UseVisualStyleBackColor = true;
            this.BtnCFuncionario.Click += new System.EventHandler(this.BtnConsFunc_Click);
            // 
            // btnLogoff
            // 
            this.btnLogoff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLogoff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogoff.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogoff.ForeColor = System.Drawing.Color.Black;
            this.btnLogoff.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnLogoff.Location = new System.Drawing.Point(30, 443);
            this.btnLogoff.Name = "btnLogoff";
            this.btnLogoff.Size = new System.Drawing.Size(116, 33);
            this.btnLogoff.TabIndex = 33;
            this.btnLogoff.Text = "Logoff";
            this.btnLogoff.UseVisualStyleBackColor = true;
            this.btnLogoff.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnCCardapio
            // 
            this.btnCCardapio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCCardapio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCCardapio.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCCardapio.ForeColor = System.Drawing.Color.Black;
            this.btnCCardapio.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCCardapio.Location = new System.Drawing.Point(30, 92);
            this.btnCCardapio.Name = "btnCCardapio";
            this.btnCCardapio.Size = new System.Drawing.Size(116, 33);
            this.btnCCardapio.TabIndex = 34;
            this.btnCCardapio.Text = "Cons. Card";
            this.btnCCardapio.UseVisualStyleBackColor = true;
            this.btnCCardapio.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // btnCPedido
            // 
            this.btnCPedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCPedido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCPedido.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCPedido.ForeColor = System.Drawing.Color.Black;
            this.btnCPedido.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCPedido.Location = new System.Drawing.Point(30, 404);
            this.btnCPedido.Name = "btnCPedido";
            this.btnCPedido.Size = new System.Drawing.Size(116, 33);
            this.btnCPedido.TabIndex = 35;
            this.btnCPedido.Text = "Consul. Ped";
            this.btnCPedido.UseVisualStyleBackColor = true;
            this.btnCPedido.Click += new System.EventHandler(this.btnCPedido_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Orange;
            this.panel1.Controls.Add(this.btninicio);
            this.panel1.Controls.Add(this.bntCliente);
            this.panel1.Controls.Add(this.btnCPedido);
            this.panel1.Controls.Add(this.btnCCardapio);
            this.panel1.Controls.Add(this.btnLogoff);
            this.panel1.Controls.Add(this.BtnCFuncionario);
            this.panel1.Controls.Add(this.btnCCliente);
            this.panel1.Controls.Add(this.btnFornecedor);
            this.panel1.Controls.Add(this.btnOrdemDePedido);
            this.panel1.Controls.Add(this.bntCardapio);
            this.panel1.Controls.Add(this.btnEstoque);
            this.panel1.Controls.Add(this.btnFuncionario);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(-6, -4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(172, 611);
            this.panel1.TabIndex = 28;
            // 
            // btninicio
            // 
            this.btninicio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btninicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btninicio.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btninicio.ForeColor = System.Drawing.Color.Black;
            this.btninicio.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btninicio.Location = new System.Drawing.Point(30, 14);
            this.btninicio.Name = "btninicio";
            this.btninicio.Size = new System.Drawing.Size(116, 33);
            this.btninicio.TabIndex = 36;
            this.btninicio.Text = "Início";
            this.btninicio.UseVisualStyleBackColor = true;
            this.btninicio.Click += new System.EventHandler(this.btninicio_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Orange;
            this.panel2.Controls.Add(this.button3);
            this.panel2.Location = new System.Drawing.Point(163, -13);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(670, 55);
            this.panel2.TabIndex = 29;
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.Orange;
            this.button3.Image = global::LanchoneteNaturalApp.Properties.Resources.icons8_delete_30;
            this.button3.Location = new System.Drawing.Point(624, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(44, 43);
            this.button3.TabIndex = 32;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_3);
            // 
            // ucConsultarPedidos3
            // 
            this.ucConsultarPedidos3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucConsultarPedidos3.Location = new System.Drawing.Point(165, 41);
            this.ucConsultarPedidos3.Name = "ucConsultarPedidos3";
            this.ucConsultarPedidos3.Size = new System.Drawing.Size(666, 495);
            this.ucConsultarPedidos3.TabIndex = 43;
            // 
            // ucConsultarPedidos2
            // 
            this.ucConsultarPedidos2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucConsultarPedidos2.Location = new System.Drawing.Point(165, 40);
            this.ucConsultarPedidos2.Name = "ucConsultarPedidos2";
            this.ucConsultarPedidos2.Size = new System.Drawing.Size(666, 496);
            this.ucConsultarPedidos2.TabIndex = 42;
            // 
            // ucConsultarPedidos1
            // 
            this.ucConsultarPedidos1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucConsultarPedidos1.Location = new System.Drawing.Point(166, 40);
            this.ucConsultarPedidos1.Name = "ucConsultarPedidos1";
            this.ucConsultarPedidos1.Size = new System.Drawing.Size(664, 496);
            this.ucConsultarPedidos1.TabIndex = 40;
            // 
            // ucPedidoCliente1
            // 
            this.ucPedidoCliente1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucPedidoCliente1.Location = new System.Drawing.Point(166, 40);
            this.ucPedidoCliente1.Name = "ucPedidoCliente1";
            this.ucPedidoCliente1.Size = new System.Drawing.Size(666, 496);
            this.ucPedidoCliente1.TabIndex = 39;
            // 
            // ucFornecedor1
            // 
            this.ucFornecedor1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucFornecedor1.Location = new System.Drawing.Point(166, 40);
            this.ucFornecedor1.Name = "ucFornecedor1";
            this.ucFornecedor1.Size = new System.Drawing.Size(666, 496);
            this.ucFornecedor1.TabIndex = 38;
            // 
            // ucEstoque2
            // 
            this.ucEstoque2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucEstoque2.Location = new System.Drawing.Point(166, 40);
            this.ucEstoque2.Name = "ucEstoque2";
            this.ucEstoque2.Size = new System.Drawing.Size(665, 496);
            this.ucEstoque2.TabIndex = 37;
            // 
            // ucEstoque1
            // 
            this.ucEstoque1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucEstoque1.Location = new System.Drawing.Point(166, 105);
            this.ucEstoque1.Name = "ucEstoque1";
            this.ucEstoque1.Size = new System.Drawing.Size(666, 431);
            this.ucEstoque1.TabIndex = 36;
            // 
            // ucConsultaFuncionario1
            // 
            this.ucConsultaFuncionario1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucConsultaFuncionario1.Location = new System.Drawing.Point(166, 40);
            this.ucConsultaFuncionario1.Name = "ucConsultaFuncionario1";
            this.ucConsultaFuncionario1.Size = new System.Drawing.Size(665, 496);
            this.ucConsultaFuncionario1.TabIndex = 35;
            // 
            // ucCadastrofuncionario1
            // 
            this.ucCadastrofuncionario1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucCadastrofuncionario1.Location = new System.Drawing.Point(166, 40);
            this.ucCadastrofuncionario1.Name = "ucCadastrofuncionario1";
            this.ucCadastrofuncionario1.Size = new System.Drawing.Size(664, 496);
            this.ucCadastrofuncionario1.TabIndex = 34;
            // 
            // ucConsultaCliente1
            // 
            this.ucConsultaCliente1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucConsultaCliente1.Location = new System.Drawing.Point(166, 40);
            this.ucConsultaCliente1.Name = "ucConsultaCliente1";
            this.ucConsultaCliente1.Size = new System.Drawing.Size(664, 496);
            this.ucConsultaCliente1.TabIndex = 33;
            // 
            // ucCadastroCliente1
            // 
            this.ucCadastroCliente1.BackColor = System.Drawing.Color.Bisque;
            this.ucCadastroCliente1.Location = new System.Drawing.Point(166, 40);
            this.ucCadastroCliente1.Name = "ucCadastroCliente1";
            this.ucCadastroCliente1.Size = new System.Drawing.Size(664, 496);
            this.ucCadastroCliente1.TabIndex = 32;
            // 
            // ucConsultarCardapio1
            // 
            this.ucConsultarCardapio1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucConsultarCardapio1.Location = new System.Drawing.Point(166, 40);
            this.ucConsultarCardapio1.Name = "ucConsultarCardapio1";
            this.ucConsultarCardapio1.Size = new System.Drawing.Size(664, 496);
            this.ucConsultarCardapio1.TabIndex = 31;
            // 
            // ucCadastroCardapio1
            // 
            this.ucCadastroCardapio1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucCadastroCardapio1.Location = new System.Drawing.Point(166, 40);
            this.ucCadastroCardapio1.Name = "ucCadastroCardapio1";
            this.ucCadastroCardapio1.Size = new System.Drawing.Size(666, 496);
            this.ucCadastroCardapio1.TabIndex = 30;
            // 
            // ucMain1
            // 
            this.ucMain1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucMain1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ucMain1.BackgroundImage")));
            this.ucMain1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ucMain1.Location = new System.Drawing.Point(166, 41);
            this.ucMain1.Name = "ucMain1";
            this.ucMain1.Size = new System.Drawing.Size(667, 495);
            this.ucMain1.TabIndex = 44;
            // 
            // FrmTelaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(830, 536);
            this.Controls.Add(this.ucMain1);
            this.Controls.Add(this.ucConsultarPedidos3);
            this.Controls.Add(this.ucConsultarPedidos2);
            this.Controls.Add(this.ucConsultarPedidos1);
            this.Controls.Add(this.ucPedidoCliente1);
            this.Controls.Add(this.ucFornecedor1);
            this.Controls.Add(this.ucEstoque2);
            this.Controls.Add(this.ucEstoque1);
            this.Controls.Add(this.ucConsultaFuncionario1);
            this.Controls.Add(this.ucCadastrofuncionario1);
            this.Controls.Add(this.ucConsultaCliente1);
            this.Controls.Add(this.ucCadastroCliente1);
            this.Controls.Add(this.ucConsultarCardapio1);
            this.Controls.Add(this.ucCadastroCardapio1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmTelaPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnFuncionario;
        private System.Windows.Forms.Button btnEstoque;
        private System.Windows.Forms.Button bntCardapio;
        private System.Windows.Forms.Button btnOrdemDePedido;
        private System.Windows.Forms.Button btnFornecedor;
        private System.Windows.Forms.Button bntCliente;
        private System.Windows.Forms.Button btnCCliente;
        private System.Windows.Forms.Button BtnCFuncionario;
        private System.Windows.Forms.Button btnLogoff;
        private System.Windows.Forms.Button btnCCardapio;
        private System.Windows.Forms.Button btnCPedido;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btninicio;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button3;
        private UserControls.UCCadastroCardapio ucCadastroCardapio1;
        private UserControls.UCConsultarCardapio ucConsultarCardapio1;
        private UserControls.UCCadastroCliente ucCadastroCliente1;
        private UserControls.UCConsultaCliente ucConsultaCliente1;
        private UserControls.UCCadastrofuncionario ucCadastrofuncionario1;
        private UserControls.UCConsultaFuncionario ucConsultaFuncionario1;
        private UserControls.UCEstoque ucEstoque1;
        private UserControls.UCEstoque ucEstoque2;
        private UserControls.UCFornecedor ucFornecedor1;
        private UserControls.UCPedidoCliente ucPedidoCliente1;
        private UserControls.UCConsultarPedidos ucConsultarPedidos1;
        private UserControls.UCConsultarPedidos ucConsultarPedidos2;
        private UserControls.UCConsultarPedidos ucConsultarPedidos3;
        private UserControls.UCMain ucMain1;
    }
}