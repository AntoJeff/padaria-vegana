﻿namespace LanchoneteNaturalApp.Telas.Frms
{
    partial class FrmPagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucPagamento1 = new LanchoneteNaturalApp.Telas.UserControls.UCPagamento();
            this.SuspendLayout();
            // 
            // ucPagamento1
            // 
            this.ucPagamento1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ucPagamento1.Location = new System.Drawing.Point(-1, 1);
            this.ucPagamento1.Name = "ucPagamento1";
            this.ucPagamento1.Size = new System.Drawing.Size(666, 431);
            this.ucPagamento1.TabIndex = 0;
            // 
            // FrmPagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 431);
            this.Controls.Add(this.ucPagamento1);
            this.Name = "FrmPagamento";
            this.Text = "Pagamento";
            this.ResumeLayout(false);

        }

        #endregion

        private UserControls.UCPagamento ucPagamento1;
    }
}