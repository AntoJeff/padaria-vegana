﻿using LanchoneteNaturalApp.Telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LanchoneteNaturalApp.Telas.Frms
{
    public partial class FrmSplash : Form
    {
        private void FrmSplash_Load(object sender, EventArgs e)
        {
           
            Task.Factory.StartNew(() =>
            {
               
                System.Threading.Thread.Sleep(4000);

                Invoke(new Action(() =>
                {
                   
                    FrmTelaPrincipal frm = new FrmTelaPrincipal();
                    frm.Show();
                    Hide();
                }));
            });

        }
    }
    }

