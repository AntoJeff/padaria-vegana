﻿using LanchoneteNaturalApp.Telas.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LanchoneteNaturalApp.Telas.Frms
{
    public partial class FrmTelaPrincipal : Form
    {
        private object lbllogo;

        public FrmTelaPrincipal()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            btnCCardapio.Height = btnCCardapio.Height;
            ucCadastroCardapio1.BringToFront();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnFuncionario_Click(object sender, EventArgs e)
        {
            btnFuncionario.Height = btnFuncionario.Height;
            ucCadastrofuncionario1.BringToFront();
        }

        private void button3_Click_2(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ucCompraProdutoEst1_Load(object sender, EventArgs e)
        {

        }

        private void Cad_Cliente_Click(object sender, EventArgs e)
        {
            bntCliente.Height = bntCliente.Height;
            ucCadastroCliente1.BringToFront();
        } 

        private void BtnCompra_Click(object sender, EventArgs e)
        {
        }



        private void button2_Click(object sender, EventArgs e)
        {
            btnCCliente.Height = btnCCliente.Height;
            ucConsultaCliente1.BringToFront();
        }

        private void BtnConsFunc_Click(object sender, EventArgs e)
        {
            BtnCFuncionario.Height = BtnCFuncionario.Height;
            ucConsultaFuncionario1.BringToFront();

        }

        private void btnEstoque_Click(object sender, EventArgs e)
        {
            btnEstoque.Height = btnEstoque.Height;
            ucEstoque1.BringToFront();
        }

        private void BtnFornecedor_Click(object sender, EventArgs e)
        {
            btnFornecedor.Height = btnFornecedor.Height;
            ucFornecedor1.BringToFront();
        }

        private void BtnPagamento_Click(object sender, EventArgs e)
        {
           
        }

        private void BtnOrdemDePedido_Click(object sender, EventArgs e)
        {
            btnOrdemDePedido.Height = btnOrdemDePedido.Height;
            ucPedidoCliente1.BringToFront();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FrmLogin login = new FrmLogin();
            login.Show();
            this.Hide();
        }

        private void ucPagamento1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            btnCCardapio.Height = btnCCardapio.Height;
            ucConsultarCardapio1.BringToFront();
        }

        private void btnCPedido_Click(object sender, EventArgs e)
        {
            btnCPedido.Height = btnCPedido.Height;
            ucConsultarPedidos1.BringToFront();
            
        }

        private void UCComprar_Load(object sender, EventArgs e)
        {
                    }

        private void btninicio_Click(object sender, EventArgs e)
        {
            btninicio.Height = btninicio.Height;
            ucMain1.BringToFront();
        }

        private void button3_Click_3(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
