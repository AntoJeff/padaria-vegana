﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Cliente;
using LanchoneteNaturalApp.BD.Classes.Pagamento;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCPagamento : UserControl
    {
        public UCPagamento()
        {
            InitializeComponent();
            combocliente();
        }

        private void UCFolhaPagamento_Load(object sender, EventArgs e)
        {

        }
        public void combocliente()
        {
            ClienteBusiness busi = new ClienteBusiness();
            List<ClienteDTO> listar = busi.Consulta();
            cbcliente.ValueMember = nameof(ClienteDTO.Id_cliente);
            cbcliente.DisplayMember = nameof(ClienteDTO.Nome);
            cbcliente.DataSource = listar;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int nota = Convert.ToInt32(txtcpfnota.Text);
            PagamentoDTO dto = new PagamentoDTO();
            dto.Cpf_nota = nota;
            dto.Id_cliente = 1;
            dto.Id_pedido = 1;
            dto.Parcelas = cbparcela.Text;
            dto.Forma_pagamento = cbpagamento.Text;
            PagamentoBusiness busi = new PagamentoBusiness();
            busi.Salvar(dto);
            MessageBox.Show("Ok", "Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
    }
}
