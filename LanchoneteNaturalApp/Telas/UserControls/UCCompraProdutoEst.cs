﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Fornecedor;
using LanchoneteNaturalApp.BD.Classes.Pedido;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCCompraProdutoEst : UserControl
    {
        public UCCompraProdutoEst()
        {
            InitializeComponent();
            Comboforncedor();
        }

        public void Comboforncedor()
        {
            FornecedorBusiness busi = new FornecedorBusiness();
            List<FornecedorDTO> Listar = busi.Listar();
            cbofornecedor.ValueMember = nameof(FornecedorDTO.Id_fornecedor);
            cbofornecedor.DisplayMember = nameof(FornecedorDTO.Nome);
            cbofornecedor.DataSource = Listar;
            
        }
        private void button1_Click(object sender, EventArgs e)
        { 
            int quant = Convert.ToInt32(txtquantidade.Text);
            PedidoCompraDTO dto = new PedidoCompraDTO();
            dto.Fornecedor = cbofornecedor.SelectedText;
            dto.Produto = txtproduto.Text;
            dto.Quantidade = quant;
            dto.Id_fornecedor_comprar = 1;
            PedidoCompraBusiness busi = new PedidoCompraBusiness();
            busi.Salvar(dto);
            MessageBox.Show("Ok", "Produto", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void UCCompraProdutoEst_Load(object sender, EventArgs e)
        {

        }
    }
}
