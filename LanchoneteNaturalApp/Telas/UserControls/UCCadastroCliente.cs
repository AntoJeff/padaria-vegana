﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Cliente;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCCadastroCliente : UserControl
    {
        public UCCadastroCliente()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void UCCadastroCliente_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {
 
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        { 
            int celular = Convert.ToInt32(txtcelular.Text);
            int cpf = Convert.ToInt32(txtcpf.Text); 
             int cep = Convert.ToInt32(txtcep.Text); 
            int tel = Convert.ToInt32(txttelefone.Text);
            ClienteDTO dto = new ClienteDTO();
            dto.Nome = txtnome.Text;
            dto.Sobrenome = txtsobrenome.Text;
            dto.Email = txtemail.Text;
            dto.Celular =celular;
            dto.Sobrenome = txtsobrenome.Text;
            dto.Cpf =cpf;
            dto.Complemento = txtcomplemento.Text;
            dto.Dia = Convert.ToInt32(cbdia.SelectedItem);
            dto.Mes = Convert.ToInt32(cbmes.SelectedItem);
            dto.Ano = Convert.ToInt32(cbano.SelectedItem);
            dto.Endereco = txtendereço.Text;
            dto.Cep = cep;
            dto.Telefone =tel;
            ClienteBusiness busi = new ClienteBusiness();
            busi.Salvar(dto);
            MessageBox.Show("Ok", "Cliente", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);


        }

        private void Remover_Click(object sender, EventArgs e)
        {
            ClienteDTO dto = new ClienteDTO();
            dto.Nome = txtnome.Text;
            dto.Sobrenome = txtsobrenome.Text;
            dto.Email = txtemail.Text;
            dto.Celular = Convert.ToInt32(txtcelular.Text);
            dto.Sobrenome = txtsobrenome.Text;
            dto.Cpf = Convert.ToInt32(txtcpf.Text);
            dto.Complemento = txtcomplemento.Text;
            dto.Dia = Convert.ToInt16(cbdia.Text);
            dto.Mes = Convert.ToInt16(cbmes.Text);
            dto.Ano = Convert.ToInt16(cbano.Text);
            dto.Endereco = txtendereço.Text;
            dto.Cep = Convert.ToInt32(txtcep.Text);
            dto.Telefone = Convert.ToInt32(txttelefone.Text);
            ClienteBusiness busi = new ClienteBusiness();
            busi.Alterar(dto);
            MessageBox.Show("ok");
        }
    }
}
