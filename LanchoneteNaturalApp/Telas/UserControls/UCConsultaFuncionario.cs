﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Funcionario;
using LanchoneteNaturalApp.BD.Classes.Funcionarios;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCConsultaFuncionario : UserControl
    {
        public UCConsultaFuncionario()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FuncionarioBusiness busi = new FuncionarioBusiness();
            List<FuncionariosDTO> Listar = busi.Listar();

            dgvconsultafuncionario.AutoGenerateColumns = false;
            dgvconsultafuncionario.DataSource = Listar;
        }
    }
}
