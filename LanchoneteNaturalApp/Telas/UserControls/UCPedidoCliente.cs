﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Pedido;
using LanchoneteNaturalApp.BD.Classes.Estoque;
using LanchoneteNaturalApp.BD.Classes;
using LanchoneteNaturalApp.Telas.Frms;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCPedidoCliente : UserControl
    {
        public UCPedidoCliente()
        {
            InitializeComponent();
            ComboProduto();
        }

        private void ComboProduto()
        {
            EstoqueBusiness business = new EstoqueBusiness();
            List<EstoqueDTO> lista = business.Listar();

            cbproduto.ValueMember = nameof(EstoqueDTO.Id_estoque);
            cbproduto.DisplayMember = nameof(EstoqueDTO.Nome);
            cbproduto.DataSource = lista;
        }

        private void button1_Click(object sender, EventArgs e)
        {
          

        }

        private void cbproduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
  //int quant = Convert.ToInt32(txtquantidade.Text);
  //          int tel = Convert.ToInt32(txttelefone.Text);
  //          PedidoDTO dto = new PedidoDTO();
  //          dto.Produto = cbproduto.SelectedText;
  //          dto.Quantidade = quant;
  //          dto.Observacao = txtobservacao.Text;
  //          dto.Telefone = tel;
  //          dto.Id_cardapio_pedido = 1;
  //          PedidoBusiness busi = new PedidoBusiness();
  //          busi.Salvar(dto);
  //          List<PedidoDTO> Listar = busi.Listar();

  //          dgvCarrinho.AutoGenerateColumns = false;
  //          dgvCarrinho.DataSource = Listar;

  //          MessageBox.Show("ok");
        }

        private void cbproduto_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            FrmPagamento pagamento = new FrmPagamento();
            pagamento.Show();
            this.Hide();
        }
    }
}
