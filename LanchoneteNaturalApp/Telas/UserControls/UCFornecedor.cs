﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Fornecedor;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCFornecedor : UserControl
    {
        public UCFornecedor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int tel = Convert.ToInt32(txttelefone.Text); 
            int cnpj = Convert.ToInt32(txtcnpj.Text);
            FornecedorDTO dto = new FornecedorDTO();
            dto.Nome = txtnome.Text;
            dto.Email = txtemail.Text;
            dto.Contato = txtcontato.Text;
            dto.Observacao = txtobservacao.Text;
            dto.Telefone = tel;
            dto.Cnpj = cnpj;
            FornecedorBusiness busi = new FornecedorBusiness();
            busi.Salvar(dto);
            MessageBox.Show("Ok", "Fornecedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
    }
}
