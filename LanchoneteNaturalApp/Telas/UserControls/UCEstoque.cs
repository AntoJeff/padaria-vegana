﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Estoque;
using LanchoneteNaturalApp.BD.Classes;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCEstoque : UserControl
    {
        public UCEstoque()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            EstoqueBusiness busi = new EstoqueBusiness();
            List<EstoqueDTO> Listar = busi.Listar();

            dgvConsultaEstoque.AutoGenerateColumns = false;
            dgvConsultaEstoque.DataSource = Listar;
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
