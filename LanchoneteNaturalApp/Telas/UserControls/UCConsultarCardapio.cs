﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Cardapio;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCConsultarCardapio : UserControl
    {
        public UCConsultarCardapio()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CardapioBusiness busi = new CardapioBusiness();
            List<CardapioDTO> Listar = busi.Listar();
            dgvCardapio.AutoGenerateColumns = false;
            dgvCardapio.DataSource = Listar;

        }
    }
}
