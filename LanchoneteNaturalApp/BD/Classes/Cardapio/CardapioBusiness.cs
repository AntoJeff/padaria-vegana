﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Cardapio
{
    class CardapioBusiness
    {
        public int Salvar(CardapioDTO dto)
        {
            CardapioDatabase db = new CardapioDatabase();
            return db.Salvar(dto);
        }
        public List<CardapioDTO> Listar()
        {
            CardapioDatabase db = new CardapioDatabase();
            return db.Listar();
        }
        public void Remover(int Id_cardapio)
        {
            CardapioDatabase db = new CardapioDatabase();
            db.Remover(Id_cardapio);
        }
    }
}
