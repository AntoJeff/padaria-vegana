﻿using LanchoneteNaturalApp.BD.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Cardapio
{
    class CardapioDatabase
    {
        public int Salvar(CardapioDTO dto)
        {
            string script = @"insert into tb_cardapio(nr_estoque_quantidade,nm_produto,vl_valor,ds_tipo) 
                                values(@nr_estoque_quantidade,@nm_produto,@vl_valor,@ds_tipo)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nr_estoque_quantidade", dto.Id_qnt_estoque));
            parms.Add(new MySqlParameter("nm_produto", dto.Produto));
            parms.Add(new MySqlParameter("vl_valor", dto.Valor));
            parms.Add(new MySqlParameter("ds_tipo", dto.Tipo));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public List<CardapioDTO> Listar()
        {
            string script = @"select * from tb_cardapio";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<CardapioDTO> listar = new List<CardapioDTO>();
            while (reader.Read())
            {
                CardapioDTO dto = new CardapioDTO();
                dto.Id_cardapio = reader.GetInt32("id_cardapio");
                dto.Id_qnt_estoque = reader.GetInt32("nr_estoque_quantidade");
                dto.Produto = reader.GetString("nm_produto");
                dto.Valor = reader.GetDouble("vl_valor");
                dto.Tipo = reader.GetString("ds_tipo");
                listar.Add(dto);

            }
            reader.Close();
            return listar;
        }
        public void Remover(int Id_cardapio)
        {
            string script = @"DELETE FROM id_cardapio WHERE id_cardapio = @id_cardapio";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cardapio", Id_cardapio));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    }
}

        
    
