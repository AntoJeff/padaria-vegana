﻿using LanchoneteNaturalApp.BD.Base;
using LanchoneteNaturalApp.BD.Classes.Funcionarios;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Funcionario
{
    class FuncionarioDatabase
    {

        public int Salvar(FuncionariosDTO dto)
        {
            string script = @"insert into tb_funcionario(nm_usuario ,ds_senha ,nm_nome ,nr_rg ,nr_cpf ,nm_dia,nm_mes,nm_ano,nm_endereco ,nr_tel ,nm_complemento,ds_cargo) 
                            values(@nm_usuario ,@ds_senha ,@nm_nome ,@nr_rg ,@nr_cpf ,@nm_dia,@nm_mes,@nm_ano,@nm_endereco ,@nr_tel ,@nm_complemento,@ds_cargo)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("nr_rg", dto.Rg));
            parms.Add(new MySqlParameter("nr_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("nm_dia", dto.Dia));
            parms.Add(new MySqlParameter("nm_mes", dto.Mes));
            parms.Add(new MySqlParameter("nm_ano", dto.Ano));
            parms.Add(new MySqlParameter("nm_endereco", dto.Endereco));
            parms.Add(new MySqlParameter("nr_tel", dto.Telefone));
            parms.Add(new MySqlParameter("nm_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("ds_cargo", dto.Cargo));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public List<FuncionariosDTO> Listar()
        {
            string script = @"select * from tb_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FuncionariosDTO> listar = new List<FuncionariosDTO>();
            while (reader.Read())
            {
                FuncionariosDTO dto = new FuncionariosDTO();
                dto.Id_funcionario = reader.GetInt32("id_funcionarios");
                dto.Nome = reader.GetString("nm_nome");
                dto.Usuario = reader.GetString("nm_usuario");
                dto.Senha = reader.GetString("ds_senha");
                dto.Rg = reader.GetString("nr_rg");
                dto.Cpf = reader.GetInt32("nr_cpf");
                dto.Dia = reader.GetInt32("nm_dia");
                dto.Mes = reader.GetInt32("nm_mes");
                dto.Ano = reader.GetInt32("nm_ano");
                dto.Endereco = reader.GetString("nm_endereco");
                dto.Telefone = reader.GetInt32("nr_tel");
                dto.Complemento = reader.GetString("nm_complemento");
                dto.Cargo = reader.GetInt32("ds_cargo");
                listar.Add(dto);
            }
            reader.Close();
            return listar;
        } 
        public void Alterar(FuncionariosDTO dto)
        {
            string script = @"update tb_funcionario 
                               set nm_usuario        = @nm_usuario,
                                   ds_senha          = @ds_senha,
                                   nm_nome           = @nm_nome,
                                   nr_rg             = @nr_rg,
                                   nr_cpf            = @nr_cpf, 
                                   nm_dia            = @nm_dia,
                                   nm_mes            = @nm_mes,
                                   nm_ano            = @nm_ano, 
                                   nm_endereco       = @nm_endereco, 
                                   nr_tel            = @nr_tel,
                                   nm_complemento    = @nm_complemento                        
                                   where id_funcionario = @id_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("nr_rg", dto.Rg));
            parms.Add(new MySqlParameter("nr_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("nm_dia", dto.Dia));
            parms.Add(new MySqlParameter("nm_mes", dto.Mes));
            parms.Add(new MySqlParameter("nm_ano", dto.Ano));
            parms.Add(new MySqlParameter("nm_endereco", dto.Endereco));
            parms.Add(new MySqlParameter("nr_tel", dto.Telefone));
            parms.Add(new MySqlParameter("nm_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("ds_cargo", dto.Cargo));
            Database db = new Database();
            db.ExecuteInsertScriptWithPk(script, parms);

        }
        public void Remover(int id_funcionario)
        {
            string script = @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", id_funcionario));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

    }
}
