﻿using LanchoneteNaturalApp.BD.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Pedido
{
    class PedidoCompraDatabase
    {
        public int Salvar(PedidoCompraDTO dto)
        {
            string script = @"insert into tb_compra_produto(nm_fornecedor,nm_produto,nm_quantidade,id_fornecedor_compra)  
                              values(@nm_fornecedor,@nm_produto,@nm_quantidade,@id_fornecedor_compra)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", dto.Fornecedor));
            parms.Add(new MySqlParameter("nm_produto", dto.Produto));
            parms.Add(new MySqlParameter("nm_quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("id_fornecedor_compra", dto.Id_fornecedor_comprar));

            Database db = new Database();

            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<PedidoCompraDTO> Listar()
        {
            string script = @"select * from tb_compra_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<PedidoCompraDTO> lista = new List<PedidoCompraDTO>();
            while (reader.Read())
            {
                PedidoCompraDTO dto = new PedidoCompraDTO();
                dto.Id_compra_produto = reader.GetInt32("id_compra_produto");
                dto.Fornecedor = reader.GetString("nm_fornacedor");
                dto.Produto = reader.GetString("nm_produto");
                dto.Quantidade = reader.GetInt32("nm_quantidade");
                dto.Id_fornecedor_comprar = reader.GetInt32("id_fornecedor_compra");
                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }
        
    }
}
