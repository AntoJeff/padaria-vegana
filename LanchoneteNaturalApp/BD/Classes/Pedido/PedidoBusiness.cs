﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO dto)
        {
            PedidoDatabase db = new PedidoDatabase();
            return db.Salvar(dto);
        }
        public List<PedidoDTO> Listar()
        {
            PedidoDatabase db = new PedidoDatabase();
            return db.Listar();
        }
        public void Remover(int id_pedido)
        {
            PedidoDatabase db = new PedidoDatabase();
            db.Remover(id_pedido);
        }
        public List<PedidoView> Consultar(string cliente)
        {
            PedidoDatabase db = new PedidoDatabase();
           return db.Consultar(cliente);

        }
    }
}
