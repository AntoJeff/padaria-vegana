﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Pedido
{
    class PedidoView
    {
        public int Id_item { get; set; }
        public int Id_pedido { get; set; }
        public int Id_cardapio { get; set; }
        public int Quantidade { get; set; }
        public string Nome { get; set; }
        public string Produto { get; set; }
    }
}
