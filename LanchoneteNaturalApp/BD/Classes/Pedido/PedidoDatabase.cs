﻿using LanchoneteNaturalApp.BD.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Pedido
{
    class PedidoDatabase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"insert into tb_pedido(id_cliente)
                               value(id_cliente)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.Id_cliente));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public List<PedidoDTO> Listar()
        {
            string script = @"select * from tb_pedido";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<PedidoDTO> listar = new List<PedidoDTO>();
            while (reader.Read())
            {
                PedidoDTO dto = new PedidoDTO();
                dto.Id_pedido = reader.GetInt32("id_pedido");
                dto.Id_cliente = reader.GetInt32("id_cliente");
                listar.Add(dto);
            }
            reader.Close();

            return listar;
        }
        public void Remover(int id_pedido)
        {
            string script = @"DELETE FROM id_pedido WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", id_pedido));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<PedidoView> Consultar(string cliente)
        {
            string script = @"SELECT * FRO MVW_PRODUT WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoView> lista = new List<PedidoView>();
            while (reader.Read())
            {
                PedidoView dto = new PedidoView();
                dto.Id_pedido = reader.GetInt32("id_pedido");
                dto.Id_item = reader.GetInt32("id_item");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Quantidade = reader.GetInt32("ds_quantidade");
                dto.Id_cardapio = reader.GetInt32("id_cardapio");
                dto.Produto = reader.GetString("nm_produto");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


    } 

}
