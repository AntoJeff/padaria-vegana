﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Fornecedor
{
    class FornecedorDTO
    {
        public int Id_fornecedor { get; set; }
        public string Nome { get; set; }
        public int Cnpj { get; set; }
        public int Telefone { get; set; }
        public string Observacao { get; set; }
        public string Email { get; set; }
        public string Contato { get; set; }
    }
}
