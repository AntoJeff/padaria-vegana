﻿using LanchoneteNaturalApp.BD.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Estoque
{
    class EstoqueDatabase
    {
        public int Salvar(EstoqueDTO dto)
        {
            string script = @" insert into tb_estoque (nm_produto,nr_quantidade,id_fornecedor)
                                 values(@nm_produto,@nr_quantidade,id_fornecedor)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("nr_quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("id_fornecedor", dto.Id_fornecedor));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public List<EstoqueDTO> Listar()
        {
            string script = @"select * from tb_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                //EstoqueDTO dto = new EstoqueDTO();
                //dto.Id_estoque = reader.GetInt32("id_estoque");
                ////dto.Nome = reader.GetString("nm_produto");
                //dto.Quantidade = reader.GetInt32("nr_quantidade");
                //dto.Id_fornecedor = reader.GetInt32("id_fornecedor");
                //lista.Add(dto);

            }
            reader.Close();
            return lista;
        }
        public void Remover(int id_estoque)
        {
            string script = @"DELETE FROM tb_estoque WHERE id_estoque = @id_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", id_estoque));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
